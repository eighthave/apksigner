/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.apksig;

import com.android.apksig.apk.ApkFormatException;
import com.android.apksig.apk.ApkSigningBlockNotFoundException;
import com.android.apksig.apk.ApkUtils;
import com.android.apksig.apk.ApkUtilsLite;
import com.android.apksig.internal.apk.ApkSigningBlockUtils;
import com.android.apksig.internal.apk.SignatureInfo;
import com.android.apksig.internal.apk.v1.V1SchemeConstants;
import com.android.apksig.internal.apk.v1.V1SchemeVerifier;
import com.android.apksig.internal.apk.v2.V2SchemeConstants;
import com.android.apksig.internal.apk.v3.V3SchemeConstants;
import com.android.apksig.internal.zip.CentralDirectoryRecord;
import com.android.apksig.internal.zip.LocalFileRecord;
import com.android.apksig.util.DataSource;
import com.android.apksig.util.DataSources;
import com.android.apksig.zip.ZipFormatException;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static com.android.apksig.internal.apk.v1.V1SchemeConstants.MANIFEST_ENTRY_NAME;

/**
 * Dump signature blocks from provided APK file to use in reproducible builds.
 *
 * <p>Use {@link Builder} to obtain instances of this verifier.
 *
 * @see <a href="https://source.android.com/security/apksigning/index.html">Application Signing</a>
 */
public class SignatureDumper {

    private final File mApkFile;
    private final DataSource mApkDataSource;

    private SignatureDumper(File apkFile, DataSource apkDataSource) {
        mApkFile = apkFile;
        mApkDataSource = apkDataSource;
    }

    /**
     * @throws IOException           if an I/O error is encountered while reading the APK
     * @throws ApkFormatException    if the APK is malformed
     * @throws IllegalStateException if this dumper's configuration is missing required
     *                               information.
     */
    public Result dump() throws IOException, ApkFormatException,
            ApkSigningBlockUtils.SignatureNotFoundException,
            IllegalStateException {
        Closeable in = null;
        try {
            DataSource apk;
            if (mApkDataSource != null) {
                apk = mApkDataSource;
            } else if (mApkFile != null) {
                RandomAccessFile f = new RandomAccessFile(mApkFile, "r");
                in = f;
                apk = DataSources.asDataSource(f, 0, f.length());
            } else {
                throw new IllegalStateException("APK not provided");
            }
            return dump(apk);
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * Dumps all the APK's signatures, one per file, into a directory..
     *
     * @param apk APK file contents
     * @throws IOException        if an I/O error is encountered while reading the APK
     * @throws ApkFormatException if the APK is malformed
     */
    private Result dump(DataSource apk)
            throws IOException, ApkFormatException, ApkSigningBlockUtils.SignatureNotFoundException {

        ApkUtils.ZipSections zipSections;
        try {
            zipSections = ApkUtils.findZipSections(apk);
        } catch (ZipFormatException e) {
            throw new ApkFormatException("Malformed APK: not a ZIP archive", e);
        }

        Result result = new Result();

        List<CentralDirectoryRecord> cdRecords =
                V1SchemeVerifier.parseZipCentralDirectory(apk, zipSections);

        // get ApplicatonID and versionCode for output dirs like `fdroid signatures`
        String packageName = ApkUtils.getPackageNameFromBinaryAndroidManifest(
                getAndroidManifestFromApk(apk, zipSections, cdRecords));
        int versionCode = ApkUtils.getVersionCodeFromBinaryAndroidManifest(
                getAndroidManifestFromApk(apk, zipSections, cdRecords));
        Path path = Paths.get(
                mApkFile.getAbsolutePath() + "-signatures",
                packageName,
                String.valueOf(versionCode));
        File outDir = path.toFile();
        if (!outDir.mkdirs() && !outDir.isDirectory()) {
            path = Paths.get(
                    Files.createTempDirectory("apksigner").toString(),
                    path.getName(path.getNameCount() - 3).toString(),
                    packageName,
                    String.valueOf(versionCode));
            outDir = path.toFile();
            if (!outDir.mkdirs()) {
                throw new IOException("No writable directory!");
            }
        }
        System.out.println("Writing signatures to " + outDir);

        // Find JAR manifest and signature block files.
        CentralDirectoryRecord manifestEntry = null;
        long cdStartOffset = zipSections.getZipCentralDirectoryOffset();
        final String metaInf = "META-INF/";
        for (CentralDirectoryRecord cdRecord : cdRecords) {
            String entryName = cdRecord.getName();
            if (!entryName.startsWith(metaInf)) {
                continue;
            }
            if ((manifestEntry == null) && (V1SchemeConstants.MANIFEST_ENTRY_NAME.equals(entryName))) {
                manifestEntry = cdRecord;
            } else if (entryName.endsWith(".SF")) {
                // Obtain the signature file from the APK
                try {
                    byte[] sigFileBytes =
                            LocalFileRecord.getUncompressedData(apk, cdRecord, cdStartOffset);
                    File sigFile = new File(outDir,
                            entryName.substring(metaInf.length()));
                    FileOutputStream fileOutputStream = new FileOutputStream(sigFile);
                    fileOutputStream.write(sigFileBytes);
                    fileOutputStream.close();
                } catch (ZipFormatException e) {
                    throw new ApkFormatException("Malformed ZIP entry: " + entryName, e);
                }
            } else if ((entryName.endsWith(".RSA"))
                    || (entryName.endsWith(".DSA"))
                    || (entryName.endsWith(".EC"))) {
                try {
                    byte[] sigBlockBytes =
                            LocalFileRecord.getUncompressedData(apk, cdRecord, cdStartOffset);
                    File sigBlock = new File(outDir,
                            entryName.substring(metaInf.length()));
                    FileOutputStream fileOutputStream = new FileOutputStream(sigBlock);
                    fileOutputStream.write(sigBlockBytes);
                    fileOutputStream.close();
                } catch (ZipFormatException e) {
                    throw new ApkFormatException("Malformed ZIP entry: " + entryName, e);
                }
            }
        }
        if (manifestEntry == null) {
            // TODO result.addError(Issue.JAR_SIG_NO_MANIFEST);
            return result;
        }

        // Parse the JAR manifest and check that all JAR entries it references exist in the APK.
        byte[] manifestBytes;
        try {
            manifestBytes =
                    LocalFileRecord.getUncompressedData(apk, manifestEntry, cdStartOffset);
            File manifestFile = new File(outDir, MANIFEST_ENTRY_NAME.substring(metaInf.length()));
            FileOutputStream fileOutputStream = new FileOutputStream(manifestFile);
            fileOutputStream.write(manifestBytes);
            fileOutputStream.close();
        } catch (ZipFormatException e) {
            // TODO not strictly required, maybe check if it is expected?
        }

        try {
            ApkUtilsLite.ApkSigningBlock apkSigningBlockInfo =
                    ApkUtilsLite.findApkSigningBlock(apk, zipSections);
            DataSource dataSource = apkSigningBlockInfo.getContents();
            File apkSigningBlockFile = new File(outDir, "APKSigningBlock");
            FileOutputStream fileOutputStream = new FileOutputStream(apkSigningBlockFile);
            FileChannel channel = fileOutputStream.getChannel();
            channel.write(dataSource.getByteBuffer(0, (int) dataSource.size()));
            fileOutputStream.close();
            channel.close();
        } catch (ApkSigningBlockNotFoundException e) {
            // TODO not strictly required, maybe check the manifest if it is expected?
        }

        FileOutputStream fileOutputStream = null;
        FileChannel channel = null;
        try {
            ApkSigningBlockUtils.Result signingBlockResultV2 = new ApkSigningBlockUtils.Result(
                    ApkSigningBlockUtils.VERSION_APK_SIGNATURE_SCHEME_V2);
            SignatureInfo signatureInfoV2 =
                    ApkSigningBlockUtils.findSignature(apk,
                            zipSections,
                            V2SchemeConstants.APK_SIGNATURE_SCHEME_V2_BLOCK_ID,
                            signingBlockResultV2);
            File signatureV2File = new File(outDir, "signatureBlockV2");
            fileOutputStream = new FileOutputStream(signatureV2File);
            channel = fileOutputStream.getChannel();
            channel.write(signatureInfoV2.signatureBlock);
            fileOutputStream.close();
            channel.close();

            ApkSigningBlockUtils.Result signingBlockresultV3 = new ApkSigningBlockUtils.Result(
                    ApkSigningBlockUtils.VERSION_APK_SIGNATURE_SCHEME_V3);
            SignatureInfo signatureInfoV3 =
                    ApkSigningBlockUtils.findSignature(apk,
                            zipSections,
                            V3SchemeConstants.APK_SIGNATURE_SCHEME_V3_BLOCK_ID,
                            signingBlockresultV3);
            File signatureV3File = new File(outDir, "signatureBlockV3");
            fileOutputStream = new FileOutputStream(signatureV3File);
            channel = fileOutputStream.getChannel();
            channel.write(signatureInfoV3.signatureBlock);
            fileOutputStream.close();
            channel.close();
        } catch (ApkSigningBlockUtils.SignatureNotFoundException e) {
            // ignored, some APKs have no v2/v3
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            if (channel != null) {
                channel.close();
            }
        }

        return result;
    }

    private static ByteBuffer getAndroidManifestFromApk(
            DataSource apk, ApkUtils.ZipSections zipSections,
            List<CentralDirectoryRecord> cdRecords)
            throws IOException, ApkFormatException {
        try {
            return ApkSigner.getAndroidManifestFromApk(
                    cdRecords,
                    apk.slice(0, zipSections.getZipCentralDirectoryOffset()));
        } catch (ZipFormatException e) {
            throw new ApkFormatException("Failed to read AndroidManifest.xml", e);
        }
    }

    /**
     * Result of dumping APK signatures.
     */
    public static class Result {
        // TODO implement me
    }

    /**
     * Builder of {@link SignatureDumper} instances.
     */
    public static class Builder {
        private final File mApkFile;
        private final DataSource mApkDataSource;

        /**
         * Constructs a new {@code Builder} for dumping signatures from the provided APK file.
         */
        public Builder(File apk) {
            if (apk == null) {
                throw new NullPointerException("apk == null");
            }
            mApkFile = apk;
            mApkDataSource = null;
        }

        /**
         * Constructs a new {@code Builder} for dumping signatures from the provided APK file.
         */
        public Builder(DataSource apk) {
            if (apk == null) {
                throw new NullPointerException("apk == null");
            }
            mApkDataSource = apk;
            mApkFile = null;
        }

        /**
         * Returns an initialized {@link SignatureDumper}.
         */
        public SignatureDumper build() {
            return new SignatureDumper(mApkFile, mApkDataSource);
        }
    }
}
