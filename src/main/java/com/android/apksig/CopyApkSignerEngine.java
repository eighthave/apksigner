/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.apksig;

import static com.android.apksig.apk.ApkUtils.SOURCE_STAMP_CERTIFICATE_HASH_ZIP_ENTRY_NAME;
import static com.android.apksig.apk.ApkUtils.computeSha256DigestBytes;
import static com.android.apksig.internal.apk.ApkSigningBlockUtils.VERSION_APK_SIGNATURE_SCHEME_V2;
import static com.android.apksig.internal.apk.ApkSigningBlockUtils.VERSION_APK_SIGNATURE_SCHEME_V3;
import static com.android.apksig.internal.apk.ApkSigningBlockUtils.VERSION_JAR_SIGNATURE_SCHEME;

import com.android.apksig.apk.ApkFormatException;
import com.android.apksig.internal.apk.v1.V1SchemeConstants;
import com.android.apksig.internal.apk.v1.V1SchemeSigner;
import com.android.apksig.internal.apk.v2.V2SchemeConstants;
import com.android.apksig.internal.apk.v2.V2SchemeSigner;
import com.android.apksig.internal.apk.v3.V3SchemeConstants;
import com.android.apksig.internal.apk.ApkSigningBlockUtils;
import com.android.apksig.internal.util.Pair;
import com.android.apksig.util.DataSink;
import com.android.apksig.util.DataSinks;
import com.android.apksig.util.DataSource;
import com.android.apksig.util.RunnablesExecutor;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.lang.IllegalStateException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementation of {@link ApkSignerEngine} used when copying the
 * signature files into the APK rather than generating them.
 *
 * <p>Use {@link Builder} to obtain instances of this engine.
 */
public class CopyApkSignerEngine implements ApkSignerEngine {

    private final boolean mV1SigningEnabled;
    private final boolean mV2SigningEnabled;
    private final boolean mV3SigningEnabled;

    private boolean mClosed;

    /** Names of JAR entries which this engine is expected to output as part of v1 signing. */
    private Set<String> mSignatureExpectedOutputJarEntryNames = Collections.emptySet();

    /** Data of JAR entries emitted by this engine as v1 signature. */
    private final Map<String, byte[]> mEmittedSignatureJarEntryData = new HashMap<>();

    private CopyApkSignerEngine() {
	// TODO add supplied JAR Signature file names,
	Set<String> set = new HashSet<>();
	set.add("META-INF/SOVA.SF");
	set.add("META-INF/SOVA.RSA");
        set.add(V1SchemeConstants.MANIFEST_ENTRY_NAME);
	mSignatureExpectedOutputJarEntryNames = set;

	// TODO set this based on input files
	mV1SigningEnabled = true;
	mV2SigningEnabled = true;
	mV3SigningEnabled = true;
    }

    @Override
    @SuppressWarnings("AndroidJdkLibsChecker")
    public Set<String> initWith(byte[] manifestBytes, Set<String> entryNames) {
        throw new IllegalStateException("unused");
    }

    @Override
    public void setExecutor(RunnablesExecutor executor) {
        throw new IllegalStateException("unused");
    }

    @Override
    public void inputApkSigningBlock(DataSource apkSigningBlock) {
        throw new IllegalStateException("unused");
    }    

    @Override
    public InputJarEntryInstructions inputJarEntry(String entryName) {
        InputJarEntryInstructions.OutputPolicy outputPolicy =
                getInputJarEntryOutputPolicy(entryName);
        switch (outputPolicy) {
            case SKIP:
                return new InputJarEntryInstructions(InputJarEntryInstructions.OutputPolicy.SKIP);
            case OUTPUT:
                return new InputJarEntryInstructions(InputJarEntryInstructions.OutputPolicy.OUTPUT);
            case OUTPUT_BY_ENGINE:
                return new InputJarEntryInstructions(
                        InputJarEntryInstructions.OutputPolicy.OUTPUT_BY_ENGINE);
            default:
                throw new RuntimeException("Unsupported output policy: " + outputPolicy);
        }
    }

    @Override
    public InspectJarEntryRequest outputJarEntry(String entryName) {
        if (!mV1SigningEnabled) {
            // No need to inspect JAR entries when v1 signing is not enabled.
            return null;
        }
	// TODO does this need to do anything to the JAR Signature entries? Perhaps set metadata, like timestamp

        // This entry is not covered by v1 signature and isn't part of v1 signature.
        return null;
    }

    @Override
    public InputJarEntryInstructions.OutputPolicy inputJarEntryRemoved(String entryName) {
        return getInputJarEntryOutputPolicy(entryName);
    }

    @Override
    public void outputJarEntryRemoved(String entryName) {
        throw new IllegalStateException("IMPLEMENT ME!");
    }

    @Override
    public OutputJarSignatureRequest outputJarEntries()
            throws ApkFormatException, InvalidKeyException, SignatureException,
                    NoSuchAlgorithmException {
        if (!mV1SigningEnabled) {
            return null;
        }

        List<OutputJarSignatureRequest.JarEntry> sigEntries = new ArrayList<>(3);
        for (String entryName : mSignatureExpectedOutputJarEntryNames) {
            byte[] entryData = {0, 0}; // TODO include data from the files
            sigEntries.add(new OutputJarSignatureRequest.JarEntry(entryName, entryData));
            mEmittedSignatureJarEntryData.put(entryName, entryData);
        }
        return new OutputJarSignatureRequestImpl(sigEntries);
    }

    @Override
    public OutputApkSigningBlockRequest outputZipSections(
            DataSource zipEntries, DataSource zipCentralDirectory, DataSource zipEocd)
            throws IOException, InvalidKeyException, SignatureException, NoSuchAlgorithmException {
        return outputZipSectionsInternal(zipEntries, zipCentralDirectory, zipEocd, false);
    }

    @Override
    public OutputApkSigningBlockRequest2 outputZipSections2(
            DataSource zipEntries, DataSource zipCentralDirectory, DataSource zipEocd)
            throws IOException, InvalidKeyException, SignatureException, NoSuchAlgorithmException {
        return outputZipSectionsInternal(zipEntries, zipCentralDirectory, zipEocd, true);
    }

    private OutputApkSigningBlockRequestImpl outputZipSectionsInternal(
            DataSource zipEntries,
            DataSource zipCentralDirectory,
            DataSource zipEocd,
            boolean apkSigningBlockPaddingSupported)
            throws IOException, InvalidKeyException, SignatureException, NoSuchAlgorithmException {
        if (!mV2SigningEnabled && !mV3SigningEnabled) {
            return null;
        }

        // adjust to proper padding
        Pair<DataSource, Integer> paddingPair =
                ApkSigningBlockUtils.generateApkSigningBlockPadding(
                        zipEntries, apkSigningBlockPaddingSupported);
        DataSource beforeCentralDir = paddingPair.getFirst();
        int padSizeBeforeApkSigningBlock = paddingPair.getSecond();
        DataSource eocd = ApkSigningBlockUtils.copyWithModifiedCDOffset(beforeCentralDir, zipEocd);

        List<Pair<byte[], Integer>> signingSchemeBlocks = new ArrayList<>();
        ApkSigningBlockUtils.SigningSchemeBlockAndDigests v2SigningSchemeBlockAndDigests = null;
        ApkSigningBlockUtils.SigningSchemeBlockAndDigests v3SigningSchemeBlockAndDigests = null;

        // create APK Signature Scheme V2 Signature if requested
        if (mV2SigningEnabled) {
	    Pair<byte[], Integer> signingSchemeBlock = Pair.of(new byte[]{0, 0}, // TODO include actual data
							       V2SchemeConstants.APK_SIGNATURE_SCHEME_V2_BLOCK_ID);
            signingSchemeBlocks.add(signingSchemeBlock);
        }
        if (mV3SigningEnabled) {
	    Pair<byte[], Integer> signingSchemeBlock = Pair.of(new byte[]{0, 0}, // TODO include actual data
							       V3SchemeConstants.APK_SIGNATURE_SCHEME_V3_BLOCK_ID);
            signingSchemeBlocks.add(signingSchemeBlock);
        }

        // create APK Signing Block with v2 and/or v3 and/or SourceStamp blocks
        byte[] apkSigningBlock = ApkSigningBlockUtils.generateApkSigningBlock(signingSchemeBlocks);
	return new OutputApkSigningBlockRequestImpl(apkSigningBlock, padSizeBeforeApkSigningBlock);
    }

    @Override
    public void outputDone() {
	// ignored
    }

    @Override
    public void signV4(DataSource dataSource, File outputFile, boolean ignoreFailures)
            throws SignatureException {
        throw new IllegalStateException("unused");
    }

    @Override
    public boolean isEligibleForSourceStamp() {
        return false;
    }

    @Override
    public byte[] generateSourceStampCertificateDigest() throws SignatureException {
        throw new IllegalStateException("unused");
    }

    @Override
    public void close() {
	// ignored
    }

    /** Returns the output policy for the provided input JAR entry. */
    private InputJarEntryInstructions.OutputPolicy getInputJarEntryOutputPolicy(String entryName) {
        if (mSignatureExpectedOutputJarEntryNames.contains(entryName)) {
            return InputJarEntryInstructions.OutputPolicy.OUTPUT_BY_ENGINE;
        }
        if (V1SchemeSigner.isJarEntryDigestNeededInManifest(entryName)) {
            return InputJarEntryInstructions.OutputPolicy.OUTPUT;
        }
        return InputJarEntryInstructions.OutputPolicy.SKIP;
    }

    private static class OutputJarSignatureRequestImpl implements OutputJarSignatureRequest {
        private final List<JarEntry> mAdditionalJarEntries;

        private OutputJarSignatureRequestImpl(List<JarEntry> additionalZipEntries) {
            mAdditionalJarEntries =
                    Collections.unmodifiableList(new ArrayList<>(additionalZipEntries));
        }

        @Override
        public List<JarEntry> getAdditionalJarEntries() {
            return mAdditionalJarEntries;
        }

        @Override
        public void done() {
        }
    }

    @SuppressWarnings("deprecation")
    private static class OutputApkSigningBlockRequestImpl
            implements OutputApkSigningBlockRequest, OutputApkSigningBlockRequest2 {
        private final byte[] mApkSigningBlock;
        private final int mPaddingBeforeApkSigningBlock;

        private OutputApkSigningBlockRequestImpl(byte[] apkSigingBlock, int paddingBefore) {
            mApkSigningBlock = apkSigingBlock.clone();
            mPaddingBeforeApkSigningBlock = paddingBefore;
        }

        @Override
        public byte[] getApkSigningBlock() {
            return mApkSigningBlock.clone();
        }

        @Override
        public void done() {
        }

        @Override
        public int getPaddingSizeBeforeApkSigningBlock() {
            return mPaddingBeforeApkSigningBlock;
        }
    }

    /** Builder of {@link CopyApkSignerEngine} instances. */
    public static class Builder {

        /**
         * Returns a new {@code CopyApkSignerEngine} instance
         * following the pattern in {@code DefaultApkSignerEngine}.
         */
        public CopyApkSignerEngine build() {
            return new CopyApkSignerEngine();
        }
    }
}
